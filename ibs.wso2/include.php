<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use IBS\WSO2\Config;
use IBS\WSO2\Handlers;
use IBS\WSO2\Request;

Bitrix\Main\Loader::registerAutoLoadClasses(
    'ibs.wso2',
    [
        Config::class => 'lib/Config.php',
        Request::class => 'lib/Request.php',
        Handlers::class => 'lib/Handlers.php',
    ]
);
