<?php

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use IBS\WSO2\Config;

Loc::loadMessages(__FILE__);

$moduleId = 'ibs.wso2';
CModule::IncludeModule($moduleId);

$MOD_RIGHT = $APPLICATION->GetGroupRight($moduleId);
$configList = Config::getConfig();

$subTabs = [];
$allOptionsList = [];

//Пароль basic-аутентификации
$allOptionsList['ConfigSources_wso2_interface']['wso2_basic_auth_key']['url'] =
    (Option::get(
        $moduleId,
        'wso2_basic_auth_key_url',
        $configList['ConfigSources']['wso2_basic_auth_key']
    ));
$allOptionsList['ConfigSources_wso2_interface']['wso2_basic_auth_key']['name'] = 'Basic Аутентификация';

//URL интерфейсов шины
foreach ($configList['ConfigSources']['wso2_interface'] as $optionName => $optionValue) {
    $allOptionsList['ConfigSources_wso2_interface'][$optionName]['url'] =
        (Option::get(
            $moduleId,
            $optionName . '_url',
            $optionValue['url']
        ));

    $allOptionsList['ConfigSources_wso2_interface'][$optionName]['name'] =
        (Option::get(
            $moduleId,
            $optionName . '_name',
            $optionValue['name']
        ));

    $allDefaultOptionsList[$optionName . '_url'] =
        $optionValue['url'];
    $allDefaultOptionsList[$optionName . '_name'] =
        $optionValue['name'];
    $optionsHtmlTypesList[$optionName] = 'text';
}

$allDefaultOptionsList['wso2_basic_auth_key_url'] =
    $configList['ConfigSources']['wso2_basic_auth_key'];

//URL интерфейсов шины для справочников hr
foreach ($configList['ConfigSources']['wso2_hr_interface'] as $optionName => $optionValue) {
    $allOptionsList['ConfigSources_wso2_hr_interface'][$optionName]['url'] =
        (Option::get(
            $moduleId,
            $optionName . '_url',
            $optionValue['url']
        ));

    $allOptionsList['ConfigSources_wso2_hr_interface'][$optionName]['name'] =
        (Option::get(
            $moduleId,
            $optionName . '_name',
            $optionValue['name']
        ));

    $allDefaultOptionsList[$optionName . '_url'] =
        $optionValue['url'];
    $allDefaultOptionsList[$optionName . '_name'] =
        $optionValue['name'];
    $optionsHtmlTypesList[$optionName] = 'text';
}

$allDefaultOptionsList['wso2_basic_auth_key_url'] =
    $configList['ConfigSources']['wso2_basic_auth_key'];

$subTabs = [
    [
        'DIV' => strtolower('ConfigSources_wso2_interface'),
        'TAB' => GetMessage('SETTINGS_WSO2_SUBTAB'),
        'TITLE' => GetMessage('SETTINGS_HR_SUBTAB_TITLE'),
    ],
    [
        'DIV' => strtolower('ConfigSources_hr'),
        'TAB' => GetMessage('SETTINGS_HR_SUBTAB'),
        'TITLE' => GetMessage('SETTINGS_HR_SUBTAB_TITLE'),
    ],
];

$subTabControl = new CAdminViewTabControl('subTabControl', $subTabs);

$tabs = [
            [
                'DIV' => 'edit1',
                'TAB' => GetMessage('SETTINGS_WSO2_TAB'),
                'TITLE' => GetMessage('SETTINGS_WSO2_TAB_TITLE'),
            ],
            [
                'DIV' => 'edit2',
                'TAB' => GetMessage('SETTINGS_ACCESS'),
                'ICON' => 'main_settings',
                'TITLE' => GetMessage('SETTINGS_ACCESS_TAB_TITLE'),
            ],
        ];

$tabControl = new CAdminTabControl('tabControl', $tabs);

if (strlen(
        $_POST['Update'] . $_GET['RestoreDefaults']
    ) > 0 && check_bitrix_sessid() && $MOD_RIGHT >= 'W') {
    if (strlen($_GET['RestoreDefaults']) > 0) {
        foreach ($allDefaultOptionsList as $defaultOptionName => $defaultOptionValue) {
            Option::set(
                $moduleId,
                $defaultOptionName,
                $defaultOptionValue
            );
        }
    } elseif (strlen($_POST['Update']) > 0) {
        $postOptionsList = $_POST['options'];

        foreach ($postOptionsList as $strOptionName => $strOptionValue) {
            Option::set(
                $moduleId,
                $strOptionName,
                $strOptionValue
            );
        }
    }

    LocalRedirect(
        $APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" .
        urlencode(LANGUAGE_ID) . "&back_url_settings=" .
        urlencode($_REQUEST['back_url_settings']) . "&" .
        $tabControl->ActiveTabParam()
    );
} ?>

<form method="post"
      action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialcharsbx($mid) ?>&lang=<?= LANG ?>">
    <?= bitrix_sessid_post() ?>
    <?php
        $tabControl->Begin();
        $tabControl->BeginNextTab();
    ?>

    <tr>
        <td colspan="2">
            <?php
            $subTabControl->Begin();
            foreach ($allOptionsList as $optionList):
                $subTabControl->BeginNextTab(); ?>

                <table width="100%" align="center">
                    <?php
                    foreach ($optionList as $strOptionName => $strOptionValue): ?>
                        <tr>
                            <td align="right" valign="middle" width="50%">
                                <?= $strOptionName.' ('.$strOptionValue['name'].')'; ?>:
                            </td>

                            <td>
                                <input type="<?= $optionsHtmlTypesList[$strOptionName]; ?>"
                                       name="options[<?= $strOptionName . '_url'; ?>]"
                                       value="<?= $strOptionValue['url']; ?>"
                                >
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>

            <?php
            endforeach;
            $subTabControl->End();
            ?>
        </td>
    </tr>

    <?php
    $tabControl->BeginNextTab();
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/admin/group_rights.php');
    $tabControl->Buttons();
    ?>

    <script type="text/javascript">
        function RestoreDefaults() {
            if (confirm('<?= AddSlashes(
                GetMessage('MAIN_HINT_RESTORE_DEFAULTS_WARNING')
            )?>')) {
                window.location = "<?= $APPLICATION->GetCurPage(
                )?>?RestoreDefaults=Y&&lang=<?= LANG?>&mid=<?= urlencode(
                    $mid
                ) . "&" . bitrix_sessid_get(); ?>";
            }
        }
    </script>

    <input type="submit"
           name="Update"
           value="<?= GetMessage('MAIN_SAVE') ?>"
        <?= ($MOD_RIGHT < 'W') ? 'disabled' : '' ?>
    >

    <input type="reset"
           name="reset"
           value="<?= GetMessage('MAIN_RESET') ?>"
    >

    <?= bitrix_sessid_post(); ?>

    <input type="button"
           title="<?= GetMessage('MAIN_HINT_RESTORE_DEFAULTS') ?>"
           OnClick="RestoreDefaults();"
           value="<?= GetMessage('MAIN_RESTORE_DEFAULTS') ?>"
        <?= ($MOD_RIGHT < 'W') ? 'disabled' : '' ?>
    >

    <?php $tabControl->End(); ?>
</form>
