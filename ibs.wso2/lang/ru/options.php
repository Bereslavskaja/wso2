<?php

$MESS['MAIN_RESTORE_DEFAULTS'] = 'По умолчанию';
$MESS['SETTINGS_WSO2_SUBTAB'] = 'Настройки взаимодейтсия c шиной WSO2';
$MESS['SETTINGS_WSO2_SUBTAB_TITLE'] = 'Настройки взаимодейтсия c шиной WSO2';
$MESS['SETTINGS_HR_SUBTAB'] = 'Справочник HR';
$MESS['SETTINGS_HR_SUBTAB_TITLE'] = 'Интерфейсы для импорта справочника HR';
$MESS['SETTINGS_WSO2_TAB'] = 'Настройки интеграции с шиной Шины WSO2';
$MESS['SETTINGS_WSO2_TAB_TITLE'] = 'Настройки интеграции с шиной Шины WSO';
$MESS['SETTINGS_ACCESS'] = 'Доступ';
$MESS['SETTINGS_ACCESS_TAB_TITLE'] = 'Уровени доступа к модулю';
