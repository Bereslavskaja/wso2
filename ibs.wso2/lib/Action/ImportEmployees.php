<?php

namespace IBS\WSO2\Action;

use Bitrix\Main\Application;
use Bitrix\Main\DB\SqlQueryException;
use Exception;
use IBS\WSO2\Handlers;
use IBS\WSO2\Orm\Tables\EmployeesTable;
use IBS\WSO2\Orm\Tables\EO_Employees;
use Symfony\Component\Console\Output\OutputInterface;

class ImportEmployees extends AbstractImportAction
{
    private static string $actionName = 'https://url.local/oco/v1/get-employees';
    private static string $tableName = 'EmployeesTable';
    private static $ormEntityClass = EmployeesTable::class;
    private static string $mask = "|%10.10s |%-20.20s |%-20.20s |%-20.20s |%-2.2s |\n";

    /**
     * @param OutputInterface $output
     * @throws SqlQueryException
     * @throws Exception
     */
    public static function run(OutputInterface $output)
    {
        $output->writeln(self::$actionName);
        Application::getConnection()->query(
            sprintf('TRUNCATE TABLE %s;', self::$ormEntityClass::getTableName())
        );

        $collection = self::$ormEntityClass::createCollection();

        $actionsList = Handlers::getModuleSettings();
        $actionUrl =  $actionsList['wso2_interface']['employees'];
        if (!$actionUrl) {
            $actionUrl = self::$actionName;
        }
        $headers = [
            'Authorization' => $actionsList['wso2_basic_auth_key'],
        ];

        $importWso2 = self::importFromWso2(
            'GET',
            $actionUrl,
            $headers
        );
        $employeesList = $importWso2['employee'];

        while ($importWso2['lastPernr']) {
            $importWso2 = self::importFromWso2(
                'GET',
                $actionUrl . '?LastPernr=' . $importWso2['lastPernr'],
                $headers
            );
            $employeesList = array_merge($employeesList, $importWso2['employee']);
        }

        if (!empty ($employeesList)) {
            foreach ($employeesList as $element) {
                if ($element['status'] !== 'Х') {
                    if($element['birthday']) {
                        $element['birthday'] = new \DateTime($element['birthday']);
                    }

                    if($element['begindateincompany']) {
                        $element['begindateincompany'] = new \DateTime($element['begindateincompany']);
                    }

                    if ($element['sex'] == 'MALE') {
                        $element['sex'] == 'M';
                    }

                    if ($element['sex'] == 'FEMALE') {
                        $element['sex'] == 'F';
                    }

                    $collection->add($object = static::getObject($element));
                    static::print($object, $output);
                }
            }
        } else {
            $output->writeln('Нет данных');
        }

        $saveResult = $collection->save(true);
        if (!$saveResult->isSuccess()) {
            throw new Exception(
                implode("\n", $saveResult->getErrorMessages())
            );
        }
    }

    /**
     * @param array $element
     * @return EO_Employees
     */
    static function getObject(array $element): EO_Employees
    {
        return (new EO_Employees())
            ->setGuid1c(trim($element['guiD1C']))
            ->setId($element['codefl'])
            ->setActive($element['status'] !== 'X' ? 'Y' : 'N')
            ->setPersonalNumber($element['pernr'])
            ->setName($element['name'])
            ->setMiddleName($element['midname'])
            ->setLastName($element['surname'])
            ->setSex(trim($element['sex']))
            ->setEmail($element['email'])
            //UF_VACATION_DAYS  Количество отпускных дней
            ->setVacationDays(intval($element['VacationDays']))
            //UF_COMPENSATORY_DAYS / Количество отгулов
            ->setCompensatoryDays(intval($element['VDCompensatoryTimeDays']))
            //UF_DEPARTMENT id департамента&
            ->setDepartmentId($element['unit'])
            ->setBirthday(
                $element['birthday'] instanceof \DateTime ? $element['birthday']->format(
                    'd.m.Y'
                ) : ''
            )
            ->setPhone($element['phone'])
            /** нет в шине
            ->setInnerPhone($element['InterPhone'])
            ->setCellPhone($element['CellPhone'])
            ->setMobilePhone($element['MobileNumber'])
            ->setMobilePhonePublic($element['MobileNumberIsPublic'])
            */
            //WORK_POSITION /Должность
            ->setWorkPosition($element['position']['description'])
            ->setWorkStreet($element['OfficeAddress'])
            //WORK_CITY
            ->setWorkCity($element['location']['description'])
            //WORK_COMPANY / Наименование компании, вкладка работа
            ->setWorkCompany($element['subsection']['description'])
            //->setLegalName($element['LegalPerson'])
            //->setDistUser()
            ->setBossId($element['bosspernr'])
            ->setHrPartnerId($element['hrpartnerpernr'])
            //привязка к таблице HL с грейдами или строка с названием грейда
            ->setGrade($element['grade']['description'])
            ->setSecretary($element['adminpernr'])
            //->setRoom($element['Room'] ?: ($element['Building'] ?: ''))
            //->setRoomId($element['RoomId'])
            //->setWorkFax($element['Fax'])
            ->setLocation($element['location']['description'])
            ->setOfferLineManager($element['offer']['bosspernr'])
            ->setOfferMentor($element['offer']['mentorpernr'])
            ->setOfferHr($element['offer']['recruiterpernr'])
            ->setOfferFirstWorkday(
                $element['offer']['firstworkday'] instanceof \DateTime ? $element['offer']['firstworkday']->format(
                    'd.m.Y'
                ) : ''
            )
            ->setFactAddress($element['factaddress'])
            ->setWorkStartedDate(
                $element['begindateincompany'] instanceof \DateTime ? $element['begindateincompany']->format(
                    'd.m.Y'
                ) : ''
            );
    }

    /**
     * @param EO_Employees $object
     * @param OutputInterface $output
     */
    public static function print($object, OutputInterface $output)
    {
        $output->writeln(
            sprintf(
                self::$mask,
                $object->getId(),
                $object->getName(),
                $object->getMiddleName(),
                $object->getLastName(),
                $object->getActive()
            )
        );
    }
}
