<?php

namespace IBS\WSO2\Action;

use Exception;

class ImportSpecializations extends ImportToHlAction
{
    /**
     * @throws Exception
     */
    public static function run (){
        self::$actionName = 'https://url.local/accounting/v1/specializations';
        self::$tableName = 'ibs_wso2_specialization';
        self::$endpointType = 'specializations';
        self::$endpointsNameResponse = 'SpecializationsResponse';
        self::$endpointNameResponse =  'Specialization';

        parent::run();
    }
}
