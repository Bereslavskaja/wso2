<?php

namespace IBS\WSO2\Action;

use IBS\WSO2\Handlers;
use IBS\Main\Helper\HL;
use Exception;
use IBS\WSO2\Request;

abstract class ImportToHlAction
{
    /**
     * @var string
     */
    protected static string $actionName;
    /**
     * @var string
     */
    protected static string $tableName;
    /**
     * @var string
     */
    protected static string $endpointType;
    /**
     * @var string
     */
    protected static string $endpointsNameResponse;
    /**
     * @var string
     */
    protected static string $endpointNameResponse;

    public static function importFromWso2($method, $url, $headers)
    {
        $request = new Request();
        return $request->run($method, $url, $headers);
    }

    /**
     * @throws Exception
     */
    protected static function run()
    {
        $actionsList = Handlers::getModuleSettings();
        $actionUrl = $actionsList['wso2_interface'][self::$endpointType];

        if (!$actionUrl) {
            $actionUrl = self::$actionName;
        }
        $headers = [
            'Authorization' => $actionsList['wso2_basic_auth_key'],
        ];

        $importWso2 = self::importFromWso2(
            'GET',
            $actionUrl,
            $headers
        );

        $dataWso2 = array_map(function ($tag) {
            return [
                'UF_GUID_1C' => $tag['GUID1C'],
                'UF_CODE' => $tag['CODE'],
                'UF_NAME' => $tag['DESCRIPTION'],
            ];
        },
            $importWso2[self::$endpointsNameResponse]['return'][self::$endpointNameResponse]
        );

        $hlBlock = HL::getClass(self::$tableName);

        $elementsHlList = $hlBlock::query()
            ->addSelect('ID')
            ->addSelect('UF_GUID_1C')
            ->addSelect('UF_CODE')
            ->addSelect('UF_NAME')
            ->addOrder('ID', 'ASC')
            ->fetchAll();

        $columnWso2 = array_column($dataWso2, 'UF_GUID_1C');
        $columnHl = array_column($elementsHlList, 'UF_GUID_1C', 'ID');
        $deleteList = array_diff($columnHl, $columnWso2);
        $addList = array_diff($columnWso2, $columnHl);

        // удаление в таблице HL инфоблока того, что не пришло из шины
        if (!empty($deleteList)) {
            foreach ($deleteList as $key => $deleteItem) {
                $hlBlock::delete($key);
            }
        }

        // обновление, если есть различия значений элементов в шине и в таблице HL инфоблока
        foreach ($dataWso2 as $itemWso2) {
            foreach ($elementsHlList as $itemHl) {
                if (
                    $itemWso2['UF_GUID_1C'] == $itemHl['UF_GUID_1C']
                    && ($itemWso2['UF_CODE'] !== $itemHl['UF_CODE']
                        || $itemWso2['UF_NAME'] !== $itemHl['UF_NAME']
                    )
                ) {
                    $hlBlock::update(
                        $itemHl['ID'],
                        [
                            'UF_CODE' => $itemWso2['UF_CODE'],
                            'UF_NAME' => $itemWso2['UF_NAME']
                        ]
                    );
                }
            }
        }

        // добавление того, что пришло в шине, но отсутствует в таблице HL инфоблока
        if (!empty($addList)) {
            foreach ($addList as $key => $addItem) {
                $hlBlock::add(
                    [
                        'UF_GUID_1C' => $dataWso2[$key]['UF_GUID_1C'],
                        'UF_CODE' => $dataWso2[$key]['UF_CODE'],
                        'UF_NAME' => $dataWso2[$key]['UF_NAME']
                    ]
                );
            }
        }
    }
}
