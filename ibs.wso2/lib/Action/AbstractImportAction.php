<?php

namespace IBS\WSO2\Action;

use IBS\WSO2\Request;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractImportAction
{

    public static function importFromWso2($method, $url, $headers)
    {
        $request = new Request();
        $resRequest = $request->run($method, $url, $headers);

        return $resRequest;
    }

    /**
     * @param $collection
     * @param $elements
     * @param OutputInterface $output
     */
    public static function importToTable(
        $collection,
        $elements,
        OutputInterface $output
    ) {
        if (!empty($elements)) {
            while ($element = sqlsrv_fetch_array(
                $elements,
                SQLSRV_FETCH_ASSOC
            )) {
                $collection->add($object = static::getObject($element));
                static::print($object, $output);
            }
        } else {
            $output->writeln('Нет данных');
        }
    }

    abstract static function getObject(array $element);

    abstract static function print($object, OutputInterface $output);
}
