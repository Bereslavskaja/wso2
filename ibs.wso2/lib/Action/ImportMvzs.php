<?php

namespace IBS\WSO2\Action;

use Exception;

class ImportMvzs extends ImportToHlAction
{
    /**
     * @throws Exception
     */
    public static function run (){
        self::$actionName = 'https://url.local/accounting/v1/kostls';
        self::$tableName = 'ibs_wso2_mvz';
        self::$endpointType = 'kostls';
        self::$endpointsNameResponse = 'KostlsResponse';
        self::$endpointNameResponse =  'Kostl';

        parent::run();
    }
}
