<?php

namespace IBS\WSO2\Action;

use Exception;

class ImportGrades extends ImportToHlAction
{
    /**
     * @throws Exception
     */
    public static function run (){
        self::$actionName = 'https://url.local/accounting/v1/grades';
        self::$tableName = 'ibs_wso2_grade';
        self::$endpointType = 'grades';
        self::$endpointsNameResponse = 'GradesResponse';
        self::$endpointNameResponse =  'Grade';

        parent::run();
    }
}
