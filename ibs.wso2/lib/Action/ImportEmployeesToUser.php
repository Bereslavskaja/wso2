<?php

namespace IBS\WSO2\Action;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use CUser;
use IBS\Main\UserTable;
use IBS\WSO2\Orm\Tables\EmployeesTable;
use Symfony\Component\Console\Output\OutputInterface;

class ImportEmployeesToUser
{
    /**
     * @param OutputInterface $output
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public static function run(OutputInterface $output)
    {
        self::bind($output);
        self::update($output);
    }

    /**
     * @param $output
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    private static function bind(OutputInterface $output)
    {
        $employeesCollection = EmployeesTable::query()
            ->addSelect('ID')
            ->where('ACTIVE', 'Y')
            ->whereNull('USER.ID')
            ->exec()
            ->fetchCollection()
        ;

        if ($employeesCollection->count() > 0) {
            $usersCollection = UserTable::query()
                ->addSelect('ID')
                ->addSelect('LOGIN')
                //'ID сотрудника MSSQL' у пользователя в UserTable, в шине - 'codefl'
                ->addSelect('UF_CENTRAL_PERSON')
                ->whereNotNull('UF_CENTRAL_PERSON')
                ->whereIn('UF_CENTRAL_PERSON', $employeesCollection->getIdList())
                ->where('ACTIVE', 'Y')
                ->exec()
                ->fetchCollection();
            $usersMap = array_combine(
                $usersCollection->getIdList(),
                $usersCollection->getUfCentralPersonList()
            );

            foreach ($usersCollection as $obUser) {
                $xmlId = $usersMap[$obUser->getId()];
                $user = new CUser();
                $user->Update($obUser->getId(), ['XML_ID' => $xmlId]);
                $output->writeln(sprintf('Set XML_ID of user %s to %s', $obUser->getLogin(), $xmlId));
            }
        }
    }

    /**
     * @param OutputInterface $output
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    private static function update(OutputInterface $output)
    {
        $employeesCollection = EmployeesTable::query()
            ->addSelect('*')
            ->addSelect('USER.ID')
            ->addSelect('USER.LOGIN')
            ->where('ACTIVE', 'Y')
            ->where('USER.ACTIVE', 'Y')
            ->whereNotNull('USER.ID')
            ->exec()
            ->fetchCollection();

        if ($employeesCollection->count() > 0) {
            foreach ($employeesCollection as $obEmployee) {
                $userID = $obEmployee->get('USER')->get('ID');
                $userLogin = $obEmployee->get('USER')->get('LOGIN');

                $user = new CUser();
                $user->Update($userID, [
                    'NAME' => $obEmployee->getName(),
                    'SECOND_NAME' => $obEmployee->getMiddleName(),
                    'LAST_NAME' => $obEmployee->getLastName(),
                    'EMAIL' => $obEmployee->getEmail(),
                    'PERSONAL_GENDER' => $obEmployee->getSex(),
                    'PERSONAL_BIRTHDAY' => $obEmployee->getBirthday(),
                    'PERSONAL_MOBILE' => $obEmployee->getCellPhone(),
                    'PERSONAL_CITY' => $obEmployee->getLocation(),
                    'WORK_COMPANY' => $obEmployee->getWorkCompany(),
                    'WORK_POSITION' => $obEmployee->getWorkPosition(),
                    'WORK_STREET' => $obEmployee->getWorkStreet(),
                    'WORK_PHONE' => $obEmployee->getPhone(),
                    'WORK_CITY' => $obEmployee->getWorkCity() ?: '',
                    'WORK_FAX' => $obEmployee->getWorkFax(),
                    'UF_DEPARTMENT' => $obEmployee->getDepartmentId(),
                    'UF_PHONE_INNER' => $obEmployee->getInnerPhone(),
                    'UF_PERSONAL_NUMBER' => $obEmployee->getPersonalNumber(),
                    'UF_BOSS' => $obEmployee->getBossId(),
                    'UF_SECRETARY' => $obEmployee->getSecretary(),
                    'UF_GRADE' => $obEmployee->getGrade(),
                    'UF_ROOM' => $obEmployee->getRoom(),
                    'UF_ROOM_ID' => $obEmployee->getRoomId(),
                    'UF_DIST_USER' => $obEmployee->getDistUser(),
                    'UF_LEGAL_NAME' => $obEmployee->getLegalName(),
                    //'UF_MEDIATOR_MARK' => 'Y',
                    'UF_PERSONAL_MOBILE_B' => $obEmployee->getMobilePhonePublic(),
                    'UF_PERSONAL_MOBILE_S' => $obEmployee->getMobilePhone(),
                    'UF_VACATION_DAYS' => $obEmployee->getVacationDays(),
                    'UF_COMPENSATORY_DAYS' => $obEmployee->getCompensatoryDays(),
                    'UF_OFFER_LINE_MANAGER' => $obEmployee->getOfferLineManager(),
                    'UF_OFFER_MENTOR' => $obEmployee->getOfferMentor(),
                    'UF_WORK_STARTED_DATE' => $obEmployee->getWorkStartedDate() ?: $obEmployee->getOfferFirstWorkday(),
                    'UF_FACT_ADDRESS' => $obEmployee->getFactAddress(),
                    'UF_HR_PARTNER' =>  $obEmployee->getHrPartnerId(),
                ]);
                $output->writeln(sprintf('User %s updated', $userLogin));
            }
        }
    }
}