<?php

namespace IBS\WSO2;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Web\Json;
use IBS\IbsHttpClient;

class Request
{
    private const SEND_METHOD_HTTP_GET = 'GET';

    private static function decode($data)
    {
        try {
            return Json::decode($data);
        } catch (ArgumentException $exception) {
            return false;
        }
    }

    public function run($method, $url, array $headers, array $params = [])
    {
        $httpClient = new IbsHttpClient();
        $httpClient->disableSslVerification();

        foreach ($headers as $name => $value) {
            $httpClient->setHeader($name, $value);
        }

        if ($method === self::SEND_METHOD_HTTP_GET) {
            $response = $httpClient->get($url);
        }

        return static::decode($response);
    }
}
