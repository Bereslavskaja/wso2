<?php

namespace IBS\WSO2\Orm\Tables;

use Bitrix\Iblock\SectionTable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\BooleanField;
use Bitrix\Main\ORM\Fields\DateField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\SystemException;
use IBS\IBlock\IblockTable;
use IBS\Main\UserTable;

class EmployeesTable extends DataManager
{
    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'ibs_orm_wso2_employees';
    }

    /**
     * @return array
     * @throws SystemException
     * @throws ArgumentException
     * @throws LoaderException
     */
    public static function getMap()
    {
        Loader::includeModule('iblock');

        return [
            'ID' => new StringField(
                'ID',
                ['primary' => true, 'required' => true]
            ),
            'USER' => (new Reference(
                'USER',
                UserTable::class,
                Join::on('this.ID', 'ref.XML_ID')
                    ->where('ref.ACTIVE', 'Y')
            ))->configureJoinType('left'),
            'ACTIVE' => new BooleanField('ACTIVE', ['values' => ['N', 'Y']]),
            'PERSONAL_NUMBER' => new StringField('PERSONAL_NUMBER'),
            'NAME' => new StringField('NAME'),
            'MIDDLE_NAME' => new StringField('MIDDLE_NAME'),
            'LAST_NAME' => new StringField('LAST_NAME'),
            'SEX' => new StringField('SEX'),
            'EMAIL' => new StringField('EMAIL'),
            'DEPARTMENT_ID' => new StringField('DEPARTMENT_ID'),
            'DEPARTMENT' => (new Reference(
                'DEPARTMENT',
                SectionTable::class,
                Join::on('this.DEPARTMENT_ID', 'ref.CODE')
                    ->where(
                        'ref.IBLOCK_ID',
                        IblockTable::getID('departments', 'structure')
                    )
            ))->configureJoinType('left'),
            'BIRTHDAY' => new StringField('BIRTHDAY', ['required' => false]),
            'PHONE' => new StringField('PHONE'),
            'INNER_PHONE' => new StringField('INNER_PHONE'),
            'CELL_PHONE' => new StringField('CELL_PHONE'),
            'MOBILE_PHONE' => new StringField('MOBILE_PHONE'),
            'MOBILE_PHONE_PUBLIC' => new BooleanField(
                'MOBILE_PHONE_PUBLIC',
                ['values' => ['N', 'Y']]
            ),
            'WORK_POSITION' => new StringField('WORK_POSITION'),
            'WORK_STREET' => new StringField('WORK_STREET'),
            'WORK_CITY' => new StringField('WORK_CITY'),
            'WORK_COMPANY' => new StringField('WORK_COMPANY'),
            'WORK_FAX' => new StringField('WORK_FAX'),
            'DIST_USER' => new StringField('DIST_USER'),
            'LEGAL_NAME' => new StringField('LEGAL_NAME'),
            'BOSS_ID' => new StringField('BOSS_ID'),
            'BOSS' => (new Reference(
                'BOSS',
                UserTable::class,
                Join::on('this.BOSS_ID', 'ref.XML_ID')
            ))->configureJoinType('left'),
            'GRADE' => new StringField('GRADE'),
            'SECRETARY' => new StringField('SECRETARY'),
            'ROOM' => new StringField('ROOM'),
            'ROOM_ID' => new StringField('ROOM_ID'),
            'VACATION_DAYS' => new IntegerField('VACATION_DAYS'),
            'COMPENSATORY_DAYS' => new IntegerField('COMPENSATORY_DAYS'),
            'LOCATION' => new StringField('LOCATION'),
            'OFFER_LINE_MANAGER' => new StringField('OFFER_LINE_MANAGER'),
            'OFFER_MENTOR' => new StringField('OFFER_MENTOR'),
            'OFFER_HR' => new StringField('OFFER_HR'),
            'OFFER_FIRST_WORKDAY' => new DateField('OFFER_FIRST_WORKDAY'),
            'FACT_ADDRESS' => new StringField('FACT_ADDRESS'),
            'WORK_STARTED_DATE' => new DateField('WORK_STARTED_DATE'),
            'GUID1C' => new StringField('GUID1C'),
            'HR_PARTNER_ID' => new StringField('HR_PARTNER_ID'),
            'HR_PARTNER' => (new Reference(
                'HR_PARTNER',
                UserTable::class,
                Join::on('this.HR_PARTNER_ID', 'ref.XML_ID')
            ))->configureJoinType('left'),
        ];
    }
}
