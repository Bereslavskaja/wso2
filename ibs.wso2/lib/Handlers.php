<?php

namespace IBS\WSO2;

use Bitrix\Main\Config\Option;

class Handlers
{
    /**
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    //вынести в родительский конструктор
    public static function getModuleSettings(): array
    {
        $configList = Config::getConfig();
        $arWso2Interface = $configList['ConfigSources']['wso2_interface'];
        $arWso2HrInterface = $configList['ConfigSources']['wso2_hr_interface'];

        //Ключ аутентификации
        $modulConfig['wso2_basic_auth_key'] = Option::get(
            'ibs.wso2',
            'wso2_basic_auth_key_url'
        );

        //Интерфейсы шины
        foreach ($arWso2Interface as $k => $interface) {
            $modulConfig['wso2_interface'][$k] = Option::get(
                'ibs.wso2',
                $k . '_url'
            );
        }

        //Интерфейсы шины для справочников HR
        foreach ($arWso2HrInterface as $n => $interfaceHr) {
            $modulConfig['wso2_hr_interface'][$n] = Option::get(
                'ibs.wso2',
                $n . '_url'
            );
        }

        return $modulConfig;
    }
}
