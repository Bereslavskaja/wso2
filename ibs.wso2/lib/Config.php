<?php

namespace IBS\WSO2;

use IBS\IBlock\IblockTable;

class Config
{
    public static function getConfigValue($strConfigValueKey)
    {
        $configValuesList = [
            'ConfigFilePath' => '/local/modules/ibs.wso2/.settings.php',
            'IblockDepartmentsID' => IblockTable::getID(
                'departments',
                'structure'
            ),
        ];

        return $configValuesList[$strConfigValueKey];
    }

    public static function getConfig()
    {
        $strConfigFilePath = self::getConfigValue('ConfigFilePath');
        $arConfig = [];
        if (file_exists(
            $config = $_SERVER['DOCUMENT_ROOT'] . $strConfigFilePath
        )) {
            ob_start();
            $arConfig = include($config);
            ob_end_clean();
        }
        return $arConfig;
    }
}
