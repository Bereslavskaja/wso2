<?php

namespace IBS\WSO2\Agents;

use Exception;
use IBS\WSO2\Action\ImportEmployees;
use IBS\WSO2\Action\ImportEmployeesToUser;
use IBS\WSO2\Action\ImportGrades;
use IBS\WSO2\Action\ImportMvzs;
use IBS\WSO2\Action\ImportSpecializations;
use Symfony\Component\Console\Output\NullOutput;

class Import
{
    /**
     * @throws Exception
     */
    public static function run(): string
    {
        ImportEmployees::run(new NullOutput());
        ImportMvzs::run(new NullOutput());
        ImportGrades::run(new NullOutput());
        ImportSpecializations::run(new NullOutput());
        ImportEmployeesToUser::run(new NullOutput());
        return '\IBS\WSO2\Agents\Import::run();';
    }
}
