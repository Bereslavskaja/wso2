<?php

return [
    'ConfigSources' => [
        'wso2_basic_auth_key' => 'Basic YYYYYYYYY',
        'wso2_interface' => [
            'absences' => [
                    'url' => 'https://url.local/accounting/v1/absences',
                    'name' => 'Отсутствия сотрудников',
                ],
            'employees' => [
                    'url' => 'https://url.local/oco/v1/get-employees',
                    'name' => 'Сотрудники',
                ],
            'grades' => [
                    'url' => 'https://url.local/accounting/v1/grades',
                    'name' => 'Справочник грейдов',
                ],
            'kostls' => [
                    'url' => 'https://url.local/accounting/v1/kostls',
                    'name' => 'Справочник МВЗ',
                ],
            'locations' => [
                    'url' => 'https://url.local/accounting/v1/locations',
                    'name' => 'Справочник локаций',
                ],
            'organizations' => [
                    'url' => 'https://url.local/accounting/v1/organizations',
                    'name' => 'Справочник юрлиц',
                ],
            'org_structure' => [
                    'url' => 'https://url.local/oco/v1/get-orgstructure',
                    'name' => 'Оргструктура',
                ],
            'region_centers' => [
                    'url' => 'https://url.local/accounting/v1/region_centers',
                    'name' => 'Справочник РЦ',
                ],
            'rests' => [
                    'url' => 'https://url.local/accounting/v1/rests',
                    'name' => 'Остатки отпусков и отгулов',
                ],
            'specializations' => [
                    'url' => 'https://url.local/accounting/v1/specializations',
                    'name' => 'Справочник специализаций',
                ],
            'stafftables' => [
                    'url' => 'https://url.local/accounting/v1/stafftables',
                    'name' => 'Штатное расписание',
                ],
        ],
        'wso2_hr_interface' => [
            'hrpartners' => [
                    'url' => 'https://url.local/hrdirectory/v1/get-hrpartners',
                    'name' => 'Получение справочника HR партнеров',
                ],
            'hrkostls' => [
                    'url' => 'https://url.local/hrdirectory/v1/get-hrkostls',
                    'name' => 'Получение справочника HR МВЗ',
                ],
            'hrdirectors' => [
                    'url' => 'https://url.local/hrdirectory/v1/get-hrdirectors',
                    'name' => 'Получение справочника HR директоров',
                ],
        ],
    ],
];
