<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use IBS\WSO2\ORM\Tables\EmployeesTable;

if (class_exists('ibs_wso2')) {
    return;
}

class ibs_wso2 extends CModule
{
    /** @var string */
    public $MODULE_ID;

    /** @var string */
    public $MODULE_VERSION;

    /** @var string */
    public $MODULE_VERSION_DATE;

    /** @var string */
    public $MODULE_NAME;

    /** @var string */
    public $MODULE_DESCRIPTION;

    /** @var string */
    public $MODULE_GROUP_RIGHTS;

    /** @var string */
    public $PARTNER_NAME;

    /** @var string */
    public $PARTNER_URI;

    public function __construct()
    {
        Loc::loadMessages(__FILE__);

        $this->MODULE_ID = GetMessage('MODULE_ID');
        $this->MODULE_VERSION = GetMessage('MODULE_VERSION');
        $this->MODULE_VERSION_DATE = GetMessage('MODULE_VERSION_DATE');;
        $this->MODULE_NAME = GetMessage('MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = GetMessage('MODULE_GROUP_RIGHTS');
        $this->PARTNER_NAME = GetMessage('PARTNER_NAME');
        $this->PARTNER_URI = GetMessage('PARTNER_URI');
    }

    public function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->installDB();
    }

    public function doUninstall()
    {
        $this->uninstallDB();
        ModuleManager::unregisterModule($this->MODULE_ID);
    }

    public function installDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            EmployeesTable::getEntity()->createDbTable();
        }
    }

    public function uninstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            $connection = Application::getInstance()->getConnection();
            $connection->dropTable(EmployeesTable::getTableName());
        }
    }
}
