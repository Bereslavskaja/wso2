<?php

$MESS['MODULE_NAME'] = 'Модуль интеграции с шиной wso2';
$MESS['MODULE_DESCRIPTION'] = 'Модуль содержит классы и модели необходимые для интеграции с шиной wso2';
$MESS['MODULE_ID'] = 'ibs.wso2';
$MESS['MODULE_VERSION'] = '0.0.1';
$MESS['MODULE_VERSION_DATE'] = '2022-05-16';
$MESS['MODULE_GROUP_RIGHTS'] = 'N';
$MESS['PARTNER_NAME'] = '';
$MESS['PARTNER_URI'] = '';
